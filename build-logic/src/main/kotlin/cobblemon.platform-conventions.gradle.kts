plugins {
    id("cobblemon.base-conventions")
    id("com.github.johnrengelman.shadow")
}

val embedModApi: Configuration by configurations.creating
val embedMod: Configuration by configurations.creating
val embed: Configuration by configurations.creating
val bundle: Configuration by configurations.creating {
    isCanBeConsumed = false
    isCanBeResolved = true
}
configurations {
    modApi {
        extendsFrom(embedModApi)
    }
    modImplementation {
        extendsFrom(embedMod)
    }
    implementation {
        extendsFrom(embed)
    }
    include {
        extendsFrom(embed)
        extendsFrom(embedMod)
        extendsFrom(embedModApi)
    }
}

tasks {

    jar {
        archiveBaseName.set("Cobblemon-${project.name}")
        archiveClassifier.set("dev-slim")
    }

    shadowJar {
        archiveClassifier.set("dev-shadow")
        archiveBaseName.set("Cobblemon-${project.name}")
        configurations = listOf(bundle)
        mergeServiceFiles()
    }

    remapJar {
        dependsOn(shadowJar)
        inputFile.set(shadowJar.flatMap { it.archiveFile })
        archiveBaseName.set("Cobblemon-${project.name}")
        archiveVersion.set("${rootProject.version}")
    }

}